const problem4 = require("../callback4.cjs");

problem4((error, result) => {
  if (error) {
    console.log(error);
  }
  console.log(result);
});
