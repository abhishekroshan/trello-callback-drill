const fs = require("fs");

function getBoardInformation(boardId, callback) {
  setTimeout(() => {
    fs.readFile("../boards_1.json", "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      }
      //reading the file  boards_1.json
      const boards = JSON.parse(data);
      //stroing the data in boards variable

      const board = boards.forEach((element) => {
        //iterating through the boards and checking if the id matches the
        //required id
        if (element.id === boardId) {
          callback(element);
          //if board_id matches we call a callback on that element
        }
      });
    });
  }, 2000);
}

module.exports = getBoardInformation;

// const boards = require("./boards_1.json");
// //getting the json data

// function getBoardInformation(boardId, callback) {
//   setTimeout(() => {
//     const board = boards.forEach((board) => {
//       //iterating through elements of the boards
//       if (board.id === boardId) {
//         //if board_id matches..then we call the call back for that board
//         callback(board);
//       }
//     });
//   }, 2000);
// }
