const fs = require("fs");
function getListInformation(boardId, callback) {
  setTimeout(() => {
    fs.readFile("../lists_1.json", "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      }
      const lists = JSON.parse(data);
      //reading the file and storing the data in the lists variable

      const listArray = Object.keys(lists);
      //using object property to store keys

      const allLists = listArray.forEach((value) => {
        if (value === boardId) {
          callback(lists[value]);
          //if the element in keys matches the boardid
          //we call the callback in the lists object with the current value.
        }
      });
    });
  }, 2000);
}

module.exports = getListInformation;

// const listData = require("./lists_1.json");

// const lists = Object.keys(listData);

// function getListInformation(boardId, callback) {
//   setTimeout(() => {
//     const allLists = lists.forEach((value) => {
//       if (value === boardId) {
//         callback(listData[value]);
//       }
//     });
//   }, 2000);
// }
