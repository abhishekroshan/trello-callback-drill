const fs = require("fs");

function getCardsInformation(boardId, callback) {
  setTimeout(() => {
    fs.readFile("../cards_1.json", "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      }

      const card = JSON.parse(data);
      //reading the file and storing the data in the card variable

      const cardArray = Object.keys(card);
      //using object property to store keys

      const cardData = cardArray.forEach((element) => {
        if (element === boardId) {
          callback(card[element]);
          //if the element in cardarray matches the boardid
          //we call the callback in the card object with the current value
        }
      });
    });
  }, 2000);
}

module.exports = getCardsInformation;

// const cards = require("./cards_1.json");

// const cardData = Object.keys(cards);

// function getCardsInformation(boardId, callback) {
//   setTimeout(() => {
//     const card = cardData.forEach((value) => {
//       if (value === boardId) {
//         callback(cards[value]);
//       }
//     });
//   }, 3000);
// }
