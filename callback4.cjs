const fs = require("fs");

const getBoardInformation = require("./callback1.cjs");
const getListInformation = require("./callback2.cjs");
const getCardsInformation = require("./callback3.cjs");

function problem4(callback) {
  fs.readFile("../boards_1.json", "utf-8", (err, data) => {
    //reading the boards json file
    if (err) {
      console.log(err);
    }
    const allBoards = JSON.parse(data);
    //storing the data in allBoards

    let id = "";
    //variable to store the id which we have to find

    allBoards.forEach((element) => {
      if (element.name === "Thanos") {
        id = element.id;
        //if the name is thanos we store its id
      }
    });
    getBoardInformation(id, (board) => {
      callback(null, board);
      //using the id to search for the complete object
    });

    getListInformation(id, (board) => {
      callback(null, board);
      //getting the list item associated with the id

      let id = "";
      //this id is the one which we have to find in the "cards" file

      board.forEach((value) => {
        if (value.name === "Mind") {
          id = value.id;
          //if the value of cards is mind we store its id

          getCardsInformation(id, (cards) => {
            callback(null, cards);
            //calling the getcards information to find the idd
          });
        }
      });
    });
  });
}

module.exports = problem4;
