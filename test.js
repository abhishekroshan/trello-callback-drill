// callback5.cjs
const getBoardInformation = require("./callback1.cjs");
const getListsForBoard = require("./callback2.cjs");
const getCardsForList = require("./callback3.cjs");

function getMindAndSpaceInformation() {
  getBoardInformation("mcu453ed", (board) => {
    console.log("Board Information:", board);

    getListsForBoard(board.id, (lists) => {
      console.log("Lists for Thanos Board:", lists);

      const mindListID = lists.find((list) => list.name === "Mind").id;
      const spaceListID = lists.find((list) => list.name === "Space").id;

      getCardsForList(mindListID, (mindCards) => {
        console.log("Cards for the Mind List:", mindCards);
      });

      getCardsForList(spaceListID, (spaceCards) => {
        console.log("Cards for the Space List:", spaceCards);
      });
    });
  });
}

getMindAndSpaceInformation();
